let todoListContainer = document.getElementById("listContainer");
let onclickAdd = document.getElementById("check-item-id");

let todoList = [
  {
    text: "Jog around the park",
    uniqueNumber: 1,
  },
  {
    text: "10 min meditation",
    uniqueNumber: 2,
  },
  {
    text: "Read for 1 hour",
    uniqueNumber: 3,
  },
];
let todoCount = todoList.length;
let countId = document.getElementById("countValue");

countId.textContent = todoCount;

function createAndAppendTodo(todo) {
  let inputCheck = "checkbox" + todo.uniqueNumber;
  let labelId = "label" + todo.uniqueNumber;
  let todoId = "todo" + todo.uniqueNumber;

  // console.log(todoId)

  let liElement = document.createElement("li");
  liElement.classList.add("list-item");
  liElement.id = todoId;
  todoListContainer.appendChild(liElement);
  // console.log(liElement)

  let inputElement = document.createElement("input");
  inputElement.type = "checkBox";
  inputElement.id = inputCheck;
  inputElement.addEventListener("click", inputStatus);
  inputElement.classList.add("input-check");
  liElement.appendChild(inputElement);

  let labelContainer = document.createElement("div");
  labelContainer.classList.add("label-container");
  liElement.appendChild(labelContainer);

  let labelElement = document.createElement("label");
  labelElement.setAttribute("for", inputCheck);
  labelElement.id = labelId;
  labelElement.classList.add("label-element-style");
  labelElement.textContent = todo.text; // adding text by object
  labelContainer.appendChild(labelElement);

  let deleteImageContainer = document.createElement("div");
  deleteImageContainer.classList.add("delete-image-container");
  labelContainer.appendChild(deleteImageContainer);

  let deleteImage = document.createElement("img");
  deleteImage.src = "images/icon-cross.svg";
  // console.log(deleteImage)
  deleteImage.addEventListener("click", deleteFunction);
  deleteImageContainer.appendChild(deleteImage);

  function inputStatus(event) {
    let checkboxEl = document.getElementById(inputCheck);
    let labelEl = document.getElementById(labelId);

    if (checkboxEl.checked) {
      labelEl.classList.add("checked");
      countId.textContent = parseFloat(countId.textContent) - 1;
    } else {
      labelEl.classList.remove("checked");
      countId.textContent = parseFloat(countId.textContent) + 1;
    }
  }

  function deleteFunction(event) {
    event.target.parentElement.parentElement.parentElement.remove();
    todoCount = todoCount - 1;

    countId.textContent = todoCount;
  }
}

for (let todo of todoList) {
  createAndAppendTodo(todo);
}
function onaddTodo() {
  let inputEl = document.getElementById("item-text-value");
  inputElValue = inputEl.value;
  if (inputElValue === "") {
    alert("Enter to do task here");
    return;
  }

  todoCount = todoCount + 1;

  countId.textContent = todoCount;

  let newTodo = {
    text: inputElValue,
    uniqueNumber: todoCount,
  };

  createAndAppendTodo(newTodo);
  inputEl.value = "";
}
let addItemContainer1 = document.getElementById("addItemContainer");
// onclickAdd.addEventListener('change', function(){
//     if (onclickAdd.checked)
//     onaddTodo()
// })
addItemContainer1.addEventListener("submit", function (event) {
  event.preventDefault();
  onaddTodo();
});

let allStatus = document.getElementById("allState");
let activeStatus = document.getElementById("activeState");
let completedStatus = document.getElementById("completedState");
let clearCompletedItems = document.getElementById("clearCompleted");

function eventListener(event){
    if (event.target.id === 'allState'){
        allStatusFunction()
    }
    else if (event.target.id === 'activeState'){
        activeStatusFunction()
    }
    else if (event.target.id === 'completedState'){
        completedStatusFunction()
    }
    else if (event.target.id === 'clearCompleted'){
        clearCompletedItemsFunction()
    }
}

function allStatusFunction() {
    for (let each of Array.from(document.querySelectorAll("li label"))) {
      console.log(each);
      if (each) {
        each.parentElement.parentElement.style.display = "flex";
      }
    }
  }

  function activeStatusFunction() {
    for (let each of Array.from(document.querySelectorAll("li label"))) {
      if (each.classList.contains("checked")) {
        each.parentElement.parentElement.style.display = "none";
      } else {
        each.parentElement.parentElement.style.display = "flex";
      }
    }
  }

  function completedStatusFunction() {
    for (let each of Array.from(document.querySelectorAll("li label"))) {
      if (!each.classList.contains("checked")) {
        each.parentElement.parentElement.style.display = "none";
      } else {
        each.parentElement.parentElement.style.display = "flex";
      }
    }
  }

function clearCompletedItemsFunction() {
    for (let each of Array.from(document.querySelectorAll("li label"))) {
      if (each.classList.contains("checked")) {
        each.parentElement.parentElement.remove();
      }
    }
  }
  


let moonImage = document.getElementById("moonImage");
let bgElement = document.getElementById("bgContainer");
let bottomContainer = document.getElementById("bottomContainer");
let addItemContainer = document.getElementById("addItemContainer");
let itemTextValue = document.getElementById("item-text-value");
let ulContainer = document.getElementById("ulContainer");
let listContainer = document.getElementById("listContainer");
let liItems = document.querySelectorAll("li");
let itemCount = document.getElementById("itemCount");
let labelText = document.querySelectorAll("label");
console.log(liItems);
moonImage.addEventListener("click", function (event) {
  if (event.target.src.endsWith("icon-moon.svg")) {
    event.target.src = "images/icon-sun.svg";
    bgElement.style.backgroundImage = "url('images/bg-desktop-dark.jpg')";
    bottomContainer.style.backgroundColor = "black";
    addItemContainer.style.backgroundColor = "#191970";
    itemTextValue.style.backgroundColor = "#191970";
    ulContainer.style.backgroundColor = "#191970";
    listContainer.style.backgroundColor = "#191970";
    itemCount.style.color = "#DCDCDC";
    for (let eachLiItem of Array.from(liItems)) {
      eachLiItem.style.backgroundColor = "#191970";
    }
    for (let eachListItem of Array.from(labelText)) {
      eachListItem.style.color = "#ffffff";
    }
  } else {
    event.target.src = "images/icon-moon.svg";
    bgElement.style.backgroundImage = "url('images/bg-desktop-light.jpg')";
    bottomContainer.style.backgroundColor = "#ECECEC";
    addItemContainer.style.backgroundColor = "#ffffff";
    itemTextValue.style.backgroundColor = "#ffffff";
    ulContainer.style.backgroundColor = "#ffffff";
    listContainer.style.backgroundColor = "#ffffff";
    itemCount.style.color = "#303030";
    for (let eachLiItem of Array.from(liItems)) {
      eachLiItem.style.backgroundColor = "#ffffff";
    }
    for (let eachListItem of Array.from(labelText)) {
      eachListItem.style.color = "#191970";
    }
  }
});

ulContainer.addEventListener('click', eventListener)